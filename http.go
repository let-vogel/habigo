package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

var baseURL = "https://habitica.com/api/v3"

var uuid string
var token string

// HttpGet makes a GET http request
func HttpGet(endpoint string) []byte {
	httpClient := &http.Client{}

	var url strings.Builder
	url.WriteString(baseURL)
	url.WriteString(endpoint)
	req, err := http.NewRequest("GET", url.String(), nil)
	if err != nil {
		log.Fatalln(err)
	}
	req.Header.Add("x-api-user", uuid)
	req.Header.Add("x-api-key", token)
	req.Header.Add("x-client", "HabiGo-Testing")

	resp, _ := httpClient.Do(req)
	body, _ := ioutil.ReadAll(resp.Body)
	return body
}

func HttpPost(endpoint string) []byte {
	httpClient := &http.Client{}

	var url strings.Builder
	url.WriteString(baseURL)
	url.WriteString(endpoint)
	req, err := http.NewRequest("POST", url.String(), nil)
	if err != nil {
		log.Fatalln(err)
	}
	req.Header.Add("x-api-user", uuid)
	req.Header.Add("x-api-key", token)
	req.Header.Add("x-client", "HabiGo-Testing")

	resp, _ := httpClient.Do(req)
	body, _ := ioutil.ReadAll(resp.Body)
	return body	
}
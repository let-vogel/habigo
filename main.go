package main

import (
	"fmt"
)

func main() {
	success := Login()
	if success {
		todos, dailies, habits := GetTasks()
		profile := GetUser()
		Draw(dailies, habits, todos, profile)
	} else {
		fmt.Println("Incorrect login/password")
	}

}

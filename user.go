package main

import (
	"encoding/json"
	"log"
)

func GetUser() UserResponse {
	body := HttpGet("/user")

	var user UserResponse
	err := json.Unmarshal(body, &user)

	if err != nil {
		log.Fatalln(err)
	}
	return user
}

type UserResponse struct {
	Success bool `json:"success"`
	Data userData `json:"data"`
}

type userData struct {
	Name userName `json:"profile"`
	Stats userStats `json:"stats"`
}

type userStats struct {
	Hp float64 `json:"hp"`
	MaxHP float64 `json:"maxHealth"`
	Mp float64 `json:"mp"`
	MaxMP float64 `json:"maxMP"`
	Level int `json:"lvl"`
	Exp float64 `json:"exp"`
	ToNextLevel float64 `json:"toNextLevel"`
	Class string `json:"class"`
}

type userName struct {
	Name string `json:"name"`
}
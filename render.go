package main

import (
	"github.com/rivo/tview"
	"fmt"
)

var User UserResponse
var Notifications = tview.NewTextView().SetTextAlign(tview.AlignCenter).SetText("notifications").SetDynamicColors(true)

// Draw takes the list of all habits, dailies, todos and displays them in a grid of columns
func Draw(daily_tasks []Task, habit_tasks []Task, todo_tasks []Task, user UserResponse) {
	User = user

	dailies := tview.NewList().ShowSecondaryText(false).SetSelectedFunc(ScoreTask)
	for _, e := range daily_tasks {
		dailies.AddItem(e.Title, e.ID, 'e', nil)
	}

	habits := tview.NewList().ShowSecondaryText(false).SetSelectedFunc(ScoreTask)
	for _, e := range habit_tasks {
		habits.AddItem(e.Title, e.ID, 'e', nil)
	}

	todos := tview.NewList().ShowSecondaryText(false).SetSelectedFunc(ScoreTask)
	for _, e := range todo_tasks {
		todos.AddItem(e.Title, e.ID, 'e', nil)
	}

	dailies.SetBorder(true).SetTitle("Dailies")
	habits.SetBorder(true).SetTitle("Habits")
	todos.SetBorder(true).SetTitle("To do")

	grid := tview.NewGrid().
		SetRows(4, 0, 3).
		SetColumns(0, 0, 0).
		SetBorders(true).
		AddItem(drawUserInfo(user), 0, 0, 1, 3, 0, 0, false).
		AddItem(Notifications, 2, 0, 1, 3, 0, 0, false)

	// Layout for screens wider than 100 cells.
	grid.AddItem(dailies, 1, 0, 1, 1, 0, 100, false).
		AddItem(habits, 1, 1, 1, 1, 0, 100, false).
		AddItem(todos, 1, 2, 1, 1, 0, 100, false)

	if err := tview.NewApplication().SetRoot(grid, true).SetFocus(dailies).Run(); err != nil {
		panic(err)
	}
}


func drawUserInfo(u UserResponse) tview.Primitive {
	text := fmt.Sprintf("\n User: [#849bba]%v [white]Class: [#849bba]%v [white]Lvl: [#849bba]%v [white]Exp: [#849bba]%.2f/%v \n [red]♥ [white]HP [#849bba]%.2f/%v  [#2e78db]★ [white]MP: [#849bba]%v/%v", 
	u.Data.Name.Name, u.Data.Stats.Class, u.Data.Stats.Level, u.Data.Stats.Exp, u.Data.Stats.ToNextLevel, u.Data.Stats.Hp, u.Data.Stats.MaxHP, u.Data.Stats.Mp, u.Data.Stats.MaxMP)

	tw := tview.NewTextView().
	SetTextAlign(tview.AlignCenter).
	SetText(text).
	SetDynamicColors(true)

	flex := tview.NewFlex().
		AddItem(tw, 0, 100, false)

	return flex
}


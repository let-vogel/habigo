package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

// Login fetches user info and API access token.
func Login() bool {
	baseURL := "https://habitica.com/api/v3/user/auth/local/login"

	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Your login: ")
	login, _ := reader.ReadString('\n')
	fmt.Print("Your password: ")
	pass, _ := reader.ReadString('\n')

	requestBody, err := json.Marshal(map[string]string{
		"username": login,
		"password": pass,
	})

	if err != nil {
		log.Fatalln(err)
	}

	resp, err := http.Post(baseURL, "application/json", bytes.NewBuffer(requestBody))
	if err != nil {
		log.Fatalln(err)
	}

	defer resp.Body.Close()

	var user loginStatus
	body, err := ioutil.ReadAll(resp.Body)

	error := json.Unmarshal(body, &user)
	if error != nil {
		log.Fatalln(err)
	}

	uuid = user.Data.ID
	token = user.Data.Token

	return user.Success
}

type loginStatus struct {
	Success bool      `json:"success"`
	Data    loginData `json:"data"`
	Version string    `json:"appVersion"`
}

type loginData struct {
	ID       string `json:"id"`
	Token    string `json:"apiToken"`
	Username string `json:"username"`
}

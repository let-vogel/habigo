package main

import (
	"encoding/json"
	"log"
	"fmt"
)

// GetTasks fetch the list of all user's tasks, todos and habits.
func GetTasks()([]Task, []Task, []Task) {
	body := HttpGet("/tasks/user")

	res := ApiResponse{}
	err := json.Unmarshal(body, &res)

	var todos []Task
	var dailies []Task
	var habits []Task

	if err == nil {
		for _, task := range res.Data {
			switch task.Type {
				case "todo":
					todos = append(todos, task)
				case "daily":
					dailies = append(dailies, task)
				case "habit":
					habits = append(habits, task)
			}
		}
	} else {
		log.Fatalln(err)
	}
	return todos, dailies, habits
}
// ToggleTask takes a task by ID, marks it as "done" and displays a notification about earned gold/exp points
func ScoreTask(i int, title, taskID string, short rune) {
	url := fmt.Sprintf("/tasks/%v/score/up", taskID)
	body := HttpPost(url)

	d := drop{}
	err := json.Unmarshal(body, &d)
	if err != nil {
		log.Fatalln(err)
	}
	notification := fmt.Sprintf("[green]You got %v exp!", d.Data.NewEXP - User.Data.Stats.Exp)
	Notifications.SetText(notification)
	todos, dailies, habits := GetTasks()
	profile := GetUser()
	Draw(dailies, habits, todos, profile)
}

// func getHabits() {
// 	body := httpGet("/tasks/user")

// }
type ApiResponse struct {
	Success bool `json:"success`
	Data []Task `json:"data"`
}

type Task struct {
	Type string `json:"type"`
	Title string `json:"text"`
	Due bool `json:"completed"`
	ID string `json:"_id"`
}



type drop struct {
	Success bool `json:"success"`
	Data dropData `json:"data"`
}

type dropData struct {
	NewEXP float64 `json:"exp"`
}